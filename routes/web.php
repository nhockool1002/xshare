<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'login'], function () {
    Route::get('', 'LoginController@getLogin')->name('getLogin');
    Route::post('', 'LoginController@postLogin')->name('postLogin');
});

Route::get('/logout', 'LoginController@getLogout')->name('getLogout');

Route::group(['prefix' => '/', 'middleware' => 'checkunderconstruct'], function () {
    Route::get('', 'HomeController@getHome')->name('home');
    Route::prefix('/go')->group(function () {
        Route::get('{string}', 'LinkController@getRedirect')->name('getRedirect');    
    });

    Route::middleware('countviewpost')->group(function() {
        Route::prefix('post/{id}/{slug}')->group(function () {
            Route::get('', 'PostController@getPost')->name('getPost');
        });
    });
    Route::prefix('page/{id}/{slug}')->group(function () {
        Route::get('', 'PageController@getPage')->name('getPage');
    });
    Route::prefix('category/{slug}/{id}')->group(function () {
        Route::get('', 'CategoryController@getCategory')->name('getCategory');
    });
    Route::get('search', 'PostController@getSearch')->name('search');

    Route::prefix('report')->group(function () {
        Route::get('', 'ReportController@index')->name('report');
    });
});

Route::group(['prefix' => 'backend', 'middleware' => ['checklogin', 'checkunderconstruct']], function () {
    Route::get('', function () {
        return view('backend.dashboard.index');
    })->name('dashboard');

    Route::group(['prefix' => 'user', 'middleware' => 'checkadmin'], function () {
        Route::get('', 'UserController@getAll')->name('user');
        Route::get('edit/{id}', 'UserController@editUser')
            ->where(['id' => '[0-9]+'])
            ->name('editUser');
        Route::post('update/{id}', 'UserController@updateUser')
            ->where(['id' => '[0-9]+'])
            ->name('updateUser');
        Route::get('delete/{id}', 'UserController@deleteUser')
            ->where(['id' => '[0-9]+'])
            ->name('deleteUser');
        Route::get('add', 'UserController@addUser')
            ->name('addUser');
        Route::post('add', 'UserController@postaddUser')
            ->name('postaddUser');
        Route::get('filter', 'UserController@filterUser')
            ->name('filterUser');
        Route::post('filter', 'UserController@postfilterUser')
            ->name('postfilterUser');
        Route::get('find', 'UserController@searchUsers')
            ->name('findUser');
    });

    Route::group(['prefix' => 'category', 'middleware' => 'checkadmin'], function () {
        Route::get('', 'CategoryController@getAll')->name('category');
        Route::get('edit/{id}', 'CategoryController@editCategory')
            ->where(['id' => '[0-9]+'])
            ->name('editCategory');
        Route::get('add', 'CategoryController@addCategory')
            ->name('addCategory');
        Route::post('add', 'CategoryController@postaddCategory')
            ->name('postaddCategory');
        Route::get('edit/{id}', 'CategoryController@editCategory')
            ->where(['id' => '[0-9]+'])
            ->name('editCategory');
        Route::post('edit/{id}', 'CategoryController@posteditCategory')
            ->name('posteditCategory');
        Route::get('delete/{id}', 'CategoryController@deleteCategory')
            ->where(['id' => '[0-9]+'])
            ->name('deleteCategory');
        Route::post('add-moderator', 'CategoryController@addModerator')
            ->name('postaddModerator');
        Route::get('remove-moderator/{id}', 'CategoryController@removeModerator')
            ->name('removeModerator');
    });

    Route::prefix('/post')->group(function () {
        Route::get('', 'PostController@getAll')->name('post');
        Route::get('delete/{id}', 'PostController@deletePost')
            ->where(['id' => '[0-9]+'])
            ->name('deletePost');
        Route::get('add', 'PostController@addPost')
            ->name('addPost');
        Route::post('add', 'PostController@postaddPost')
            ->name('postaddPost');
        Route::get('edit/{id}', 'PostController@editPost')
            ->where(['id' => '[0-9]+'])
            ->name('editPost');
        Route::post('edit/{id}', 'PostController@posteditPost')
            ->where(['id' => '[0-9]+'])
            ->name('posteditPost');
        Route::get('filter', 'PostController@filterPost')
            ->name('filterPost');
        Route::post('filter', 'PostController@postfilterPost')
            ->name('postfilterPost');
    });

    Route::group(['prefix' => 'page', 'middleware' => 'checkadmin'], function () {
        Route::get('', 'PageController@getAll')->name('page');
        Route::get('add', 'PageController@addPage')->name('addPage');
        Route::post('add', 'PageController@postaddPage')->name('postaddPage');
        Route::get('edit/{id}', 'PageController@editPage')
            ->where(['id' => '[0-9]+'])
            ->name('editPage');
        Route::post('edit/{id}', 'PageController@posteditPage')
            ->where(['id' => '[0-9]+'])
            ->name('posteditPage');
        Route::get('delete/{id}', 'PageController@deletePage')
            ->where(['id' => '[0-9]+'])
            ->name('deletePage');
        Route::get('filter', 'PageController@filterPage')
            ->name('filterPage');
        Route::post('filter', 'PageController@postfilterPage')
            ->name('postfilterPage');
    });

    Route::group(['prefix' => 'menu', 'middleware' => 'checkadmin'], function () {
        Route::get('', 'MenuController@menuConfig')->name('menu');
        Route::post('', 'MenuController@postaddcategorymenu')->name('postaddcategorymenu');
        Route::post('add', 'MenuController@postaddmenucustom')->name('postaddmenucustom');
        Route::get('delete/{id}', 'MenuController@deletemenu')->name('deletemenu');
    });

    Route::prefix('/links')->group(function () {
        Route::get('', 'LinkController@getlLinkConfig')->name('link');
        Route::post('update-service', 'LinkController@updateService')->name('updateService');
        Route::post('store-service', 'LinkController@storeService')->name('storeService');
        Route::get('delete-service/{id}', 'LinkController@deleteService')->name('deleteService');
        Route::post('create-service', 'LinkController@createLink')->name('createLink');
        Route::get('delete-link/{id}', 'LinkController@deleteLink')->name('deleteLink');
    });

    Route::group(['prefix' => 'ads', 'middleware' => 'checkadmin'], function () {
        Route::get('', 'AdsController@getAll')->name('ads');
        Route::post('', 'AdsController@storeAll')->name('storeAds');
    });

    Route::group(['prefix' => 'settings', 'middleware' => 'checkadmin'], function () {
        Route::get('', 'SettingController@getIndex')->name('setting');
        Route::get('signature', 'SettingController@getSignScreen')->name('signature');
        Route::post('signature', 'SettingController@postSignature')->name('postSignature');
        Route::get('backend-credit', 'SettingController@getBackendCreditScreen')->name('backend-credit');
        Route::post('backend-credit', 'SettingController@postBackendCredit')->name('postBackendCredit');
        Route::get('backend-column', 'SettingController@getBackendColumnScreen')->name('backend-column');
        Route::post('backend-column', 'SettingController@postBackendColumn')->name('postBackendColumn');
        Route::get('frontend-column', 'SettingController@getFrontendColumnScreen')->name('frontend-column');
        Route::post('frontend-column', 'SettingController@postFrontendColumn')->name('postFrontendColumn');
        Route::get('social', 'SettingController@getSocialScreen')->name('social');
        Route::post('social', 'SettingController@postSocial')->name('postSocial');
        Route::get('code-example', 'SettingController@getCodeExampleScreen')->name('code-example');
        Route::get('report-list', 'ReportController@getReportScreen')->name('report-list');
        Route::get('deleteReport/{id}', 'ReportController@deleteReport')->name('deleteReport');
        Route::get('under-construct', 'SettingController@underConstruct')->name('underConstruct');
        Route::post('under-construct', 'SettingController@postunderConstruct')->name('postunderConstruct');
    });

    Route::group(['prefix' => 'log', 'middleware' => 'checkadmin'], function () {
        Route::get('', 'LogController@getAll')->name('log');
    });

    Route::group(['prefix' => 'filter', 'middleware' => 'checkadmin'], function () {
        Route::get('administrator','UserController@filterAdmin')->name('filterAdmin');
    });
});
