<?php

class Constant
{
    // TITLE
    const TITLE_NAME_TOP = 'XShare Community';
    const LOGIN_PAGE_TITLE = 'XShare Community Login';
    const TITLE_NAME = 'XShare - Free Resource Community';
    const ADMIN_TITLE_NAME = 'XShare - Administrator Panel';
    const URL_HOME = 'http://xshare.local/';
    const UPLOAD_PATH = \Constant::URL_HOME.'upload/posts/images/';
    const UPLOAD_PAGE_PATH = \Constant::URL_HOME.'upload/pages/images/';
    const AVATAR_PATH = \Constant::URL_HOME.'upload/users/images/';
    const FAV_ICON_PATH = 'login-assets/images/avatar-01.jpg';
    const IMG_PATH = \Constant::URL_HOME.'img/';

    // Screen
    const ADD_CATEGORY_FUNCTION = 'Add Categories';
    const EDIT_CATEGORY_FUNCTION = 'Edit Categories';
    const DELETE_CATEGORY_FUNCTION = 'Delete Categories';

    const ADD_USER_FUNCTION = 'Add Users';
    const EDIT_USER_FUNCTION = 'Edit Users';
    const DELETE_USER_FUNCTION = 'Delete Users';

    const ADD_POST_FUNCTION = 'Add Posts';
    const DELETE_POST_FUNCTION = 'Delete Posts';
    const EDIT_POST_FUNCTION = 'Edit Posts';

    const ADD_PAGE_FUNCTION = 'Add Pages';
    const DELETE_PAGE_FUNCTION = 'Delete Pages';
    const EDIT_PAGE_FUNCTION = 'Edit Pages';

    const LOGIN_USER_FUNCTION = 'Login User';

    const ADD_MENU_FUNCTION = 'Add Menu';
    const DELETE_MENU_FUNCTION = 'Delete Menu';

    const GENERAL_SERVICE_LINK_FUNCTION = 'General Config Service Link';
    const ADD_SERVICE_SHORTLINK_FUNCTION = 'Add Service Link';
    const DELETE_SERVICE_SHORTLINK_FUNCTION = 'Delete Service Link';
    const CREATE_SHORTLINK_FUNCTION = 'Create Shortlink Link';
    const DELETE_SHORTLINK_FUNCTION = 'Delete Shortlink Link';

    const ADVERTISEMENT_STORE_FUNCTION = 'Advertisement Store';

    const SETTING_SIGNATURE_FUNCTION = 'Default Signature Update';
    const SETTING_BACKEND_CREDIT_FUNCTION = 'Backend Credit Update';
    const SETTING_BACKEND_COLUMN_FUNCTION = 'Backend Column Update';
    const SETTING_FRONTEND_COLUMN_FUNCTION = 'Frontend Column Update';
    const SETTING_SOCIAL_FUNCTION = 'Social Update';

    const SETTING_UNDER_CONSTRUCTION = 'Under Construct Update';
    //PATH
    const IMAGE_THUMB_POST = 'upload/posts/images';
}
