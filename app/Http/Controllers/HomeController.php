<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\Post;
use App\Ads;
use App\Category;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function getHome()
    {
    	$menus = Menu::all();
    	$cats = Category::all();
    	$recent3post = Post::orderBy('id', 'desc')->take(3)->get();
    	$recent3post1 = Post::orderBy('id', 'desc')->take(3)->get();
    	$recent3post2 = Post::orderBy('id', 'desc')->skip(3)->take(3)->get();
    	$stickytop2post = Post::where('post_special', '1')->orderBy('id', 'desc')->take(2)->get();
    	$random7post = Post::inRandomOrder()->take(7)->get();
    	$random2book = Post::where('cat_id', '1')->inRandomOrder()->take(2)->get();
    	$digital3post = Post::where('cat_id', '4')->orderBy('id', 'desc')->take(3)->get();
    	$foreign4post = Post::where('cat_id', '3')->orderBy('id', 'desc')->take(4)->get();
    	$graphic6post = Post::where('cat_id', '6')->orderBy('id', 'desc')->take(4)->get();
        $homepage_rightside_300x250_first = Ads::where('ads_zone', 'homepage_rightside_300x250_first')->first();
        $homepage_rightside_300x250_second = Ads::where('ads_zone', 'homepage_rightside_300x250_second')->first();
    	return view('frontend.home.index', compact('menus', 'recent3post', 'stickytop2post' , 'recent3post1', 'recent3post2', 'random7post', 'random2book', 'digital3post', 'foreign4post', 'graphic6post', 'cats', 'homepage_rightside_300x250_first', 'homepage_rightside_300x250_second'));
    }
}
