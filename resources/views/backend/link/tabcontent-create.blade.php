<form method="POST" action="{{ route('createLink') }}">
	@csrf
<div class="row">
    <div class="col-md-12">
    	<div class="form-group">
            <label><h4>Title</h4></label>
                <input type='text' name="title" class="form-control border-input">
    	</div>
        <div class="form-group">
            <label><h4>Url</h4></label>
                <input type='text' name="link" class="form-control border-input">
    	</div>
    </div>
</div>
<div class="text-center">
    <button type="submit" class="btn btn-danger btn-fill btn-wd">Create Link</button>
</div>
</form>
<br>
@if(session('link_created'))
<div class="row">
    <div class="col-md-12">
    	<div class="form-group alert alert-info">
            <label style="color:#c53665"><h4>New Url Created</h4></label>
            <p>{{ \Constant::URL_HOME}}/go/{{session('link_created')}}</p>   
    	</div>
    </div>
</div>
@endif 