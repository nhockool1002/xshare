<div class="main-panel">
    @include('common.back.topsetting')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="col-md-12">
                            <br>
                            <div class="alertbox">
                                @if(count($errors) > 0)
                                    <div class="alert alert-danger">
                                        @foreach($errors->all() as $err)
                                            <i class="fa fa-times" aria-hidden="true"></i> {{$err}}<br>
                                        @endforeach
                                    </div>
                                @endif
                                @if(session('success_mesage'))
                                    <div class="alert alert-success">
                                        <i class="fa fa-check" aria-hidden="true"></i> {{session('success_mesage')}}
                                    </div>
                                @endif
                                @if(session('warning_mesage'))
                                    <div class="alert alert-danger">
                                        <i class="fa fa-times" aria-hidden="true"></i> {{session('warning_mesage')}}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="header">
                            <h4 class="title">Edit Post</h4>
                        </div>
                        <div class="content">
                            <form method="POST" action="{{ route('posteditPost',$post->id) }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input type="text" class="form-control border-input" placeholder="Post Name" name="title" value="{{ $post->post_name }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Category</label>
                                            <select name="category" class="form-control border-input">
                                                @foreach($cats as $cat)
                                                    <option value="{{ $cat->id }}" @if($cat->id == $post->cat_id) selected="selected" @endif>{{ $cat->cat_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Content</label>
                                            <textarea rows="5" class="form-control border-input" placeholder="Here can be your description" id="editor1" name="content">{{ $post->post_content }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Current Thumbnail</label>
                                            <div class="current-img">
                                                <img src="{{ asset('upload/posts/images') }}/{{ $post->post_img }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Add New Image Thumbnails</label>
                                            <small>(*) If you don't want to change thumnails can skip input</small>
                                            <input type="file" class="form-control border-input" placeholder="Thumbnail" name="thumb" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Stick Post</label>
                                            <select name="sticky" class="form-control border-input">
                                                    <option value="0" {{ $post->post_special == 0 ? 'selected="selected"' : '' }}>Không</option>
                                                    <option value="1" {{ $post->post_special == 1 ? 'selected="selected"' : '' }}>Có</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-info btn-fill btn-wd">Edit Post</button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div
    </div>
    @include('common.back.copyright')
</div>
