					<div class="col-md-4">
						<!-- ad -->
						<div class="aside-widget text-center">
							@include('frontend.ads.ads-right-bottom-widget-home')
						</div>
						<!-- /ad -->
						
						<!-- catagories -->
						<div class="aside-widget">
							@include('common.front.widget-list-category')
						</div>
						<!-- /catagories -->
						
						<!-- tags -->
						<div class="aside-widget">
							@include('common.front.widget-list-tags')
						</div>
						<!-- /tags -->
					</div>