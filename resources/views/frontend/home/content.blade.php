<!-- section -->
		<div class="section">
			<!-- container -->
			<div class="container">
				@include('frontend.home.stickpost')
				@include('frontend.home.recentpost')
				@include('frontend.home.mostreadpost')
			</div>
			<!-- /container -->
		</div>
		<!-- /section -->
		
		<!-- section -->
		<div class="section section-grey">
			<!-- container -->
			<div class="container">
				@include('frontend.home.featurepost')
			</div>
			<!-- /container -->
		</div>
		<!-- /section -->

		<!-- section -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					@include('frontend.home.bottompost')
					@include('frontend.home.bottomwidget')
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /section -->
