						<!-- ad -->
						<div class="aside-widget text-center">
							@include('frontend.ads.ads-right-widget-category')
						</div>
						<!-- /ad -->
						
						<!-- post widget -->
						<div class="aside-widget">
							@include('frontend.category.right-widget-category-1-mostpostincat')
						</div>
						<!-- /post widget -->
						
						<!-- catagories -->
						<div class="aside-widget">
							@include('common.front.widget-list-category')
						</div>
						<!-- /catagories -->
						
						<!-- tags -->
						<div class="aside-widget">
							@include('common.front.widget-list-tags')
						</div>
						<!-- /tags -->
						
						<!-- archive -->
						<div class="aside-widget">
							@include('common.front.widget-list-archive')
						</div>
						<!-- /archive -->