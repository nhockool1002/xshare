		<!-- section -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- Post content -->
					<div class="col-md-8">
						<div class="section-row sticky-container">
							<div class="main-post">
								@include('frontend.post.main-content')
							</div>
							<div class="post-shares sticky-shares">
								@include('frontend.post.social')
							</div>
						</div>

						<!-- ad -->
						<div class="section-row text-center">
							@include('frontend.ads.ads-bottom-post')
						</div>
						<!-- ad -->
						
						<!-- author -->
						<div class="section-row">
							<div class="post-author">
								@include('frontend.post.author')
							</div>
						</div>
						<!-- /author -->

						<!-- comments -->
						<div class="section-row">
							@include('frontend.post.comments')
						</div>
						<!-- /comments -->

						<!-- reply -->
						<div class="section-row">
							<!-- @include('frontend.post.form') -->
						</div>
						<!-- /reply -->
					</div>
					<!-- /Post content -->

					<!-- aside -->
					<div class="col-md-4">
						@include('frontend.post.widget')
					</div>
					<!-- /aside -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /section -->