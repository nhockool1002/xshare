<div class="wrap-post-content">
<h3 class="post-title">{{ $posts->post_name }}</h3>
<p>
    <span><small>Chia sẻ bởi </small></span><span class="meta-user">{{ $posts->user->username }}</span>
    <span class="meta-view-count"><small> ({{ $posts->view_count }} Lượt xem)</small></span>
</p>
<div class="post-content">
    {!! $posts->post_content !!}
    <br />
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="fb-like" data-href="{{ url()->current() }}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
            </div>
        </div>
    <br />
    <hr />
    <div class="form-group">
        @if(session('success_mesage'))
            <div class="alert alert-success">
                <i class="fa fa-check" aria-hidden="true"></i> {{session('success_mesage')}}
            </div>
        @endif
        @if(session('warning_mesage'))
            <div class="alert alert-danger">
                <i class="fa fa-times" aria-hidden="true"></i> {{session('warning_mesage')}}
            </div>
        @endif
        <a href="{{ route('report',['post' => $posts->post_name, 'id' => $posts->id ]) }}" class="btn btn-danger" onclick="return confirm('Chuyển sang báo cáo ?');">Báo cáo link hỏng</a>
        <a href="https://www.facebook.com/groups/kgroupdocument/" target="_blank" class="btn btn-info">Yêu cầu tài nguyên</a>
    </div>
</div>
<div class="sign-content">
	@if(isset($posts->user->sign))
		{!! $posts->user->sign !!}
	@else
		@if(!empty($settingConfig['signature']['config_setting']))
           {!! $settingConfig['signature']['config_setting'] !!}
        @endif
	@endif
</div>
</div>
