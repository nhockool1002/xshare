<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>@stack('postname') {{ \Constant::TITLE_NAME }}</title>

		<meta name="description" content="XShare Community - Cộng đồng chia sẻ tài nguyên miễn phí tất tần tật các lĩnh vực, bao gồm CNTT - GAMES - ĐỒ HOẠ - DATA SCIENCE - Big Data. Và tất cả tài nguyên đều hoàn toàn miễn phí." />

        <!-- Schema.org markup for Google+ -->
        <meta itemprop="name" content="XShare Community">
        <meta itemprop="description" content="XShare Community - Cộng đồng chia sẻ tài nguyên miễn phí tất tần tật các lĩnh vực, bao gồm CNTT - GAMES - ĐỒ HOẠ - DATA SCIENCE - Big Data. Và tất cả tài nguyên đều hoàn toàn miễn phí.">
        <meta itemprop="image" content="https://uphinhnhanh.com/images/2018/11/09/ScreenShot2018-11-09at10.02.42AM.png">

        <!-- Twitter Card data -->
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@publisher_handle">
        <meta name="twitter:title" content="XShare Community - Cộng đồng chia sẻ tài nguyên miễn phí>
        <meta name="twitter:description" content="XShare Community - Cộng đồng chia sẻ tài nguyên miễn phí tất tần tật các lĩnh vực, bao gồm CNTT - GAMES - ĐỒ HOẠ - DATA SCIENCE - Big Data. Và tất cả tài nguyên đều hoàn toàn miễn phí.">
        <meta name="twitter:creator" content="@author_handle">
        <!-- Hình ảnh mô tả cho Twitter summary card với kích thước tối thiểu 280x150px -->
        <meta name="twitter:image:src" content="https://uphinhnhanh.com/images/2018/11/09/ScreenShot2018-11-09at10.02.42AM.png">
        <!-- Open Graph data -->
        <meta property="og:title" content="XShare Community - Cộng đồng chia sẻ tài nguyên miễn phí" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="http://xshare-location.cf/" />
        <meta property="og:image" content="https://uphinhnhanh.com/images/2018/11/09/ScreenShot2018-11-09at10.02.42AM.png" />
        <meta property="og:description" content="XShare Community - Cộng đồng chia sẻ tài nguyên miễn phí tất tần tật các lĩnh vực, bao gồm CNTT - GAMES - ĐỒ HOẠ - DATA SCIENCE - Big Data. Và tất cả tài nguyên đều hoàn toàn miễn phí." />
        <link rel="icon" type="image/png" href="{{ asset(\Constant::FAV_ICON_PATH) }}"/>
		<!-- Google font -->
		<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:700%7CNunito:300,600" rel="stylesheet"> 

		<!-- Bootstrap -->
		<link type="text/css" rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}"/>

		<!-- Font Awesome Icon -->
		<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">

		<!-- Custom stlylesheet -->
		<link type="text/css" rel="stylesheet" href="{{ asset('css/style.css') }}"/>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

    </head>
